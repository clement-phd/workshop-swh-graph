# workshop-swh-graph
Code and setup to use rust graph. Made during the workshop in INRIA Rennes the 15/02/2024.

## Workshop [readme](https://hedgedoc.softwareheritage.org/diverse-workshop-query-swh-20250215#)

I've installed the dependencies and created the project. Their is an error in the installation process. The dependance `anyhow = {version="1.0.79", features=["backtrace"]}` miss in the Cargo.toml. The SWHID in the test command isn't found.