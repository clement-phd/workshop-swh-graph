use std::{fs, io};
use std::path::Path;

use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
#[cfg(feature = "parrallelize")]
use indicatif::ParallelProgressIterator;
use log::{info, error};

use dataset_preparation::pipeline::prepare_dataset;
use generate_query::pipeline::generate_all_queries;
#[cfg(feature = "parrallelize")]
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use rust_utils::tree_sitter_utils::get_tree_from_file_content;

use rust_utils::tree_sitter_utils::languages::Language;
use use_query::pipeline::get_query_results;
use use_query::run_query::query_output::QueryOutput;

use crate::errors::BenchmarkError;
use crate::instances::instance::Instance;
use crate::instances::{DatasetInstances, Instances};
use crate::monitoring::MonitoringResults;
use crate::params::{BENCHMARK_RESULTS_DIR_PATH, DATASET_PREPARATION_OUTPUT_DIR_PATH, MAX_COMMIT_TO_CHECK, QUERIES_DIR_PATH, REPO_TO_TESTS};
use crate::prepared_dataset::FileToTest;
use crate::repository::diff_file::DiffType;
use crate::repository::Repository;
use crate::results::commit_results::CommitResults;
use crate::results::file_results::FileResult;


/// check if the folder is empty. 
/// if not here, create it and return true, else return false
/// if clean is true, delete the folder and recreate it
fn create_or_clean_folder(
    path : &str,
    clean : bool,
) -> Result<bool, io::Error> {
    let path_o = Path::new(path);
    // apply the clean_results option
    if path_o.exists() {
        if path_o.is_dir() {
            if clean {
                info!("🧹 Clean the {} directory.", path_o.to_str().unwrap());
                fs::remove_dir_all(path_o)?;
            }
        } else {
            error!("The {} directory already exists, but it's not a directory, please delete it.", path_o.to_str().unwrap());
            panic!();
        }
    }

    // create the directory if it doesn't exist
    if !path_o.exists() {
        fs::create_dir_all(path_o)?;
        Ok(true)
    }else{
        Ok(false)
    }
}



pub fn benchmark_pipeline(dataset_path : &str, clean_results : bool, clean_dataset : bool) -> Result<(), BenchmarkError> {
    // --------------------- clean the folders ---------------------//
    create_or_clean_folder(&*DATASET_PREPARATION_OUTPUT_DIR_PATH, clean_dataset)?;
    create_or_clean_folder(&*QUERIES_DIR_PATH, clean_dataset)?;

    create_or_clean_folder(&*BENCHMARK_RESULTS_DIR_PATH, clean_results)?;
    
    // --------------------- Progress bar ---------------------//
    let common_bar = MultiProgress::new();
    let sty = ProgressStyle::with_template(
        "[{elapsed_precise}] {bar:40.cyan/blue} {pos:>7}/{len:7} {msg}",
    )
    .unwrap()
    .progress_chars("##-");

    // --------------------- prepare the datasets ---------------------//
    let dataset_iter = DatasetInstances::new();
    let dataset_preparation_bar = common_bar.add(ProgressBar::new(dataset_iter.count() as u64));
    dataset_preparation_bar.set_style(sty.clone());
    for dataset in DatasetInstances::new() {
        dataset_preparation_bar.set_message(format!("prepare dataset : {}", dataset.get_name()));
        
        if create_or_clean_folder(dataset.get_dataset_path(), false)? {
            prepare_dataset(
                Some(dataset.get_min_line_of_code_for_cve_code() as usize), 
                dataset_path, 
                dataset.get_dataset_path(), 
                dataset.get_language()
            )?;
            common_bar.suspend(||{
                info!("📦 the dataset instance {} has been prepared.", dataset.get_dataset_path());
            });
        }else{
            common_bar.suspend(||{
                info!("📦 the dataset instance {} has been already prepared.", dataset.get_dataset_path());
            });
        }
        dataset_preparation_bar.inc(1);
    }
    dataset_preparation_bar.finish();
    common_bar.remove(&dataset_preparation_bar);

    // --------------------- Prepare the queries ---------------------//
    let instance_iter = Instances::new();
    let queries_bar = common_bar.add(ProgressBar::new(instance_iter.len() as u64));
    queries_bar.set_style(sty.clone());
    for instance in instance_iter {
        queries_bar.set_message(format!("query for the instance : {}", instance.get_name()));
        
        if create_or_clean_folder(instance.get_queries_path(), false)? {
            common_bar.suspend(||{
                info!("📝 Generate the queries for the instance {}.", instance.get_name());
            });
            let queries = generate_all_queries(
                None,
                instance.get_queries_path(),
                None,
                &instance.get_generator(),
                Some(instance.get_language()),
            )?;
            let queries_dir_path_o = Path::new(instance.get_queries_path());
            for (_, query) in queries.iter().enumerate() {
                let cve_id = query.split("@").last().unwrap();
                let query_path_o = queries_dir_path_o.join(format!("{}.scm", cve_id).as_str());
                fs::write(query_path_o.to_str().unwrap(), query)?;
            }
        }else{
            common_bar.suspend(||{
                info!("📝 The queries for the instance {} already exists.", instance.get_name());
            });
        }

        queries_bar.inc(1);
    }
    queries_bar.finish();
    common_bar.remove(&queries_bar);

    benchmark_dataset(&common_bar, &sty)?;
    benchmark_repo(&common_bar, &sty)?;
    
    Ok(())
}

/// Benchmark the dataset
fn benchmark_dataset(
    common_bar : &MultiProgress,
    sty: &ProgressStyle,
) -> Result<(), BenchmarkError> {
    let mut instances_iter = Instances::new();
    let instances_bar = common_bar.add(ProgressBar::new(instances_iter.len() as u64));
    instances_bar.set_style(sty.clone());
    let mut actual_instance = instances_iter.next(); // Note : their is always at least one instance
    let mut actual_dataset = actual_instance.as_ref().unwrap().get_dataset_instance();
    let mut actual_files = actual_dataset.get_files();
    
    
    while actual_instance.is_some() {
        let instance = actual_instance.unwrap();
        if instance.get_dataset_instance() != actual_dataset {
            actual_dataset = instance.get_dataset_instance();
            actual_files = actual_dataset.get_files();
        }
        
        instances_bar.set_message(format!("benchmark on instance : {}", instance.get_name()));
        common_bar.suspend(||{
            info!("🔥 Start the benchmark for the instance {} with {} files.", instance.get_name(), actual_files.len());
        });

        // prepare the output folder
        create_or_clean_folder(instance.get_result_path(), false)?;

        // get the queries
        let query_paths_to_test = instance.get_all_query_paths();

        // run on files
        let file_bar = common_bar.add(ProgressBar::new(actual_files.len() as u64));
        file_bar.set_style(sty.clone());
        let instance_result_folder = Path::new(instance.get_result_path());
        let mut i = 0;
        for file in actual_files.iter() {
            let file_result_path_o = instance_result_folder.join(format!("{}.json", i).as_str());
            if file_result_path_o.exists() {
                common_bar.suspend(||{
                    info!("📂 The file {} already exists, skip it.", file_result_path_o.to_str().unwrap());
                });
                
                file_bar.inc(1);
                i += 1;
            }else{
                common_bar.suspend(||{
                    info!("📂 Run the benchmark for the file {}.", file_result_path_o.to_str().unwrap());
                });
                file_bar.set_message(format!("{} - {}", instance.get_name(), file.file_path.as_str()));


                // --------------------- Run the benchmark ---------------------//
                let file_result = process_file(
                    file,
                    query_paths_to_test.iter().map(|s| s.as_str()).collect::<Vec<_>>().as_slice(),
                    &instance,
                    &common_bar,
                    &sty,
                )?;
                
                file_result.save(file_result_path_o.to_str().unwrap())?;



                file_bar.inc(1);
                i += 1;
            }
        }
        file_bar.finish();
        common_bar.remove(&file_bar);
        
        // get the next instance
        actual_instance = instances_iter.next();

        instances_bar.inc(1);
    }
    instances_bar.finish();
    common_bar.remove(&instances_bar);
    Ok(())
}

/// benchmark the repo
fn benchmark_repo(
    common_bar : &MultiProgress,
    sty: &ProgressStyle,
) -> Result<(), BenchmarkError> {
    // create all repository and get their informations
    common_bar.suspend(||{
        info!("📥 Clone all the repositories.");
    });
    // reset the repositories to the head and get the commit hash
    let (mut all_repositories, all_hashs) = {
        let mut all_repos = Vec::new();
        let mut all_hashs = Vec::new();

        for (repo_name, hash) in REPO_TO_TESTS.iter() {
            let mut repo = Repository::clone(repo_name)?;
            repo.reset_to_head()?;
            repo.goto_commit(hash)?;
            all_repos.push(repo);
            all_hashs.push(hash.to_string());
        }
        
        
        (all_repos, all_hashs)
    };


    
    // create the progress bar
    let repo_bar = common_bar.add(ProgressBar::new(all_repositories.len() as u64));
    repo_bar.set_style(sty.clone());

    // run the benchmark on the repositories
    for (repo, initial_commit_hash ) in all_repositories.iter_mut().zip(all_hashs.into_iter()) {
        repo_bar.set_message(format!("benchmark on repo : {}", repo.name));
        common_bar.suspend(||{
            info!("! Start the benchmark for the repository {}.", repo.name);
        });
        
        // number of commit to check
        let max_commit_to_check = if MAX_COMMIT_TO_CHECK > 0 {
            MAX_COMMIT_TO_CHECK
        }else{
            repo.count_commits_from(initial_commit_hash.as_str())? as u32 - 1 // -1 because the first commit is the actual commit
        };

        let mut previous_commit_hash = initial_commit_hash;

        let commit_bar = common_bar.add(ProgressBar::new(max_commit_to_check as u64));
        commit_bar.set_style(sty.clone());

        for _ in 0..max_commit_to_check {
            // get the next commit and the files
            let (actual_commit_hash, diff_files) = repo.goto_previous_commit()?;
            commit_bar.set_message(format!("commit : {}", actual_commit_hash));
            common_bar.suspend(||{
                info!("📂 Start the benchmark for the repo {} on the commit result {}.", repo.name, repo.get_actual_commit_hash());
            });
            
            let diff_files = diff_files.into_iter()
                .filter(|diff_file| 
                    diff_file.diff_type != DiffType::Deleted
                )
                .collect::<Vec<_>>();

            // iterate on all the instance
            let instance_iter = Instances::new();
            let instance_bar = common_bar.add(ProgressBar::new(instance_iter.len() as u64));
            instance_bar.set_style(sty.clone());

            for instance in instance_iter {
                instance_bar.set_message(format!("benchmark (repo) on instance : {}", instance.get_name()));
                // check if we have to run the benchmark
                // create the results folder
                create_or_clean_folder(instance.get_repo_result_path(), false)?;
                let repo_instance_result_folder_o = Path::new(instance.get_repo_result_path());
                let repo_instance_result_path_o = repo_instance_result_folder_o.join(CommitResults::get_commit_file_name(repo.get_actual_commit_hash()));
                if repo_instance_result_path_o.exists() {
                    common_bar.suspend(||{
                        info!("📂 The commit result {} for the instance {} already exists, skip it.", repo.get_actual_commit_hash(), instance.get_name());
                    });
                }else{

                    common_bar.suspend(||{
                        info!("🔥 Start the benchmark for the instance {} with {} files on repo {}.", instance.get_name(), diff_files.len(), repo.name);
                    });
                    // get the queries
                    let query_paths_to_test = instance.get_all_query_paths();
                    // filter the files for the language
                    let diff_files = diff_files.iter()
                        .filter(|diff_file| 
                            Language::is_file_language(instance.get_language(), &diff_file.inside_file_path.as_str())
                        )
                        .map(|diff_file| diff_file.clone())
                        .collect::<Vec<_>>();

                    // run the benchmark for the files
                    let file_bar = common_bar.add(ProgressBar::new(diff_files.len() as u64));
                    file_bar.set_style(sty.clone());
                    
                    #[cfg(not(feature = "parrallelize"))]
                    let files_results : Vec<FileResult> = {
                        let mut files_results = Vec::new();
                        for diff_file in diff_files.iter() {
                            let file_path = repo.get_full_path(&diff_file);
                            file_bar.set_message(format!("file : {}", diff_file.inside_file_path));
                            let file_to_test = FileToTest {
                                file_path,
                                cve_id: "".to_string(),
                                is_infected: false,
                            };

                            let file_result = process_file(
                                &file_to_test,
                                query_paths_to_test.iter().map(|s| s.as_str()).collect::<Vec<_>>().as_slice(),
                                &instance,
                                &common_bar,
                                &sty,
                            )?;
                            files_results.push(file_result);


                            file_bar.inc(1);
                        }
                        files_results
                    };

                    #[cfg(feature = "parrallelize")]
                    let files_results : Vec<FileResult> = {
                        file_bar.set_message("Processing files ....".to_string());
                        let iter = diff_files.par_iter()
                            .map(|diff_file| {
                                let file_path = repo.get_full_path(&diff_file);
                                let file_to_test = FileToTest {
                                    file_path,
                                    cve_id: "".to_string(),
                                    is_infected: false,
                                };
                                let result_file = process_file(
                                    &file_to_test,
                                    query_paths_to_test.iter().map(|s| s.as_str()).collect::<Vec<_>>().as_slice(),
                                    &instance,
                                    &common_bar,
                                    &sty,
                                );
                                file_bar.inc(1);
                                result_file
                            });
                        iter.collect::<Result<Vec<FileResult>, BenchmarkError>>()?
                    };

                    file_bar.finish();
                    common_bar.remove(&file_bar);

                    // create the commit result
                    CommitResults::new(
                        repo.url.as_ref().unwrap(),
                        actual_commit_hash.as_str(),
                        previous_commit_hash.as_str(),
                        &instance,
                        files_results,
                        diff_files.clone(),
                    ).save(instance.get_repo_result_path())?;
                }

                instance_bar.inc(1);
            }
            instance_bar.finish();
            common_bar.remove(&instance_bar);

            previous_commit_hash = actual_commit_hash;
            commit_bar.inc(1);
        }
        commit_bar.finish();
        common_bar.remove(&commit_bar);
    }
    repo_bar.finish();
    common_bar.remove(&repo_bar);

    Ok(())
}


/// Benchmark a file with a bench of query
#[cfg(feature = "parrallelize")]
fn process_file(
    file: &FileToTest,
    query_paths_to_test: &[&str],
    instance : &Instance,
    common_bar : &MultiProgress,
    sty: &ProgressStyle,
) -> Result<FileResult, BenchmarkError> {
    let source_code_to_test = fs::read_to_string(file.file_path.as_str())?;

    let parsing_result = {
        let language_actual = instance.get_language().clone(); // Replace with your actual language
        let source_code_to_test = source_code_to_test.clone();
        MonitoringResults::monitor_thread(move || {
            get_tree_from_file_content(source_code_to_test.as_str(), &language_actual)
        })
    };

    let query_bar = common_bar.add(ProgressBar::new(query_paths_to_test.len() as u64));    
    query_bar.set_style(sty.clone());
    query_bar.set_message(format!("Processing queries for file {} ....", file.file_path.as_str()));


    let query_test_results  = query_paths_to_test.par_iter().map(
        |query_path| {
            let query_path_o = Path::new(query_path);
            let query_result = {
                let query_path = query_path_o.to_str().unwrap().to_string();
                let language_actual = instance.get_language().clone(); // Replace with your actual language
                let file_path = file.file_path.to_string();
                let source_code_to_test = source_code_to_test.clone();
                let parsing_result = parsing_result.clone();
                MonitoringResults::monitor_thread(move || {
                    get_query_results(
                        query_path.as_str(),
                        &parsing_result.0,
                        source_code_to_test.as_str(),
                        &language_actual,
                        &file_path,
                    )
                })
            };

            query_bar.inc(1);

            match query_result.0 {
                Some(query_output) => Ok((vec![query_output], query_result.1, query_path_o.to_str().unwrap())),
                None => Err(BenchmarkError::FileNotYieldQuery(file.file_path.as_str().to_string())),
            }
        }
    ).collect::<Result<Vec<(Vec<QueryOutput>, MonitoringResults, &str)>, BenchmarkError>>()?;

    query_bar.finish();
    common_bar.remove(&query_bar);

    Ok(FileResult::new(
        parsing_result.1,
        file.clone(),
        instance,
        query_test_results,
    ))
}


#[cfg(not(feature = "parrallelize"))]
fn process_file(
    file: &FileToTest,
    query_paths_to_test: &[&str],
    instance : &Instance,
    common_bar : &MultiProgress,
    sty: &ProgressStyle,
) -> Result<FileResult, BenchmarkError> {
    let source_code_to_test = fs::read_to_string(file.file_path.as_str())?;

    let parsing_result = {
        let language_actual = instance.get_language().clone(); // Replace with your actual language
        let source_code_to_test = source_code_to_test.clone();
        MonitoringResults::monitor_thread(move || {
            get_tree_from_file_content(source_code_to_test.as_str(), &language_actual)
        })
    };

    let query_bar = common_bar.add(ProgressBar::new(query_paths_to_test.len() as u64));
    query_bar.set_style(sty.clone());

    let mut query_test_results: Vec<(Vec<QueryOutput>, MonitoringResults, &str)> = Vec::new();

    for query_path in query_paths_to_test {
        query_bar.set_message(format!("query : {}", query_path));
        let query_path_o = Path::new(query_path);
        let query_result = {
            let query_path = query_path_o.to_str().unwrap().to_string();
            let language_actual = instance.get_language().clone(); // Replace with your actual language
            let file_path = file.file_path.to_string();
            let source_code_to_test = source_code_to_test.clone();
            let parsing_result = parsing_result.clone();
            MonitoringResults::monitor_thread(move || {
                get_query_results(
                    query_path.as_str(),
                    &parsing_result.0,
                    source_code_to_test.as_str(),
                    &language_actual,
                    &file_path,
                )
            })
        };

        let query_outputs = match query_result.0 {
            Some(query_output) => query_output,
            None => {
                return Err(BenchmarkError::FileNotYieldQuery(file.file_path.as_str().to_string()));
            }
        };

        query_test_results.push((vec![query_outputs], query_result.1, query_path_o.to_str().unwrap()));

        query_bar.inc(1);
    }

    query_bar.finish();
    common_bar.remove(&query_bar);

    Ok(FileResult::new(
        parsing_result.1,
        file.clone(),
        instance,
        query_test_results,
    ))
}